"""
pyinstaller -n tvt-monitor --clean --add-data config;config -i icon-tvt.ico main.py
"""

import PyInstaller.__main__
excluded_modules = [
    'scipy',
]

append_string = ''
for mod in excluded_modules:
    append_string += f'--exclude-module {mod}'

PyInstaller.__main__.run([
    '-y',
    '-p', 'src',
    'src/main.py',
    # 'src/test/Test07.py',
    # -F --onefile # -D，--onedir
    # '--onedir',
    '--onefile',
    # -w --windowed # -c，--nowindowed
    '--windowed',
    '-n', 'DogLeg',
    '-i', 'src/view/ui/res/robot.ico',
    '--exclude-module', 'scipy',
    '--exclude-module', 'mkl',
    '--exclude-module', 'matplotlib',
])
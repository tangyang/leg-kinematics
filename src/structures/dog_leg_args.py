from structures.abs_structure_args import *


class DogLegAbsStructureArgs(AbsStructureArgs):

    def _get_init_point_end(self):
        return [0, 60]

    def _get_init_links(self):
        return {
            ORG_LINK_AC: 13.0,
            LINK_CD: 27.0,
            LINK_AB: 27.0,
            LINK_BE: 35.0,
            LINK_DE: 27.0,
            LINK_EF: 30.0,
        }.copy()

    def _get_init_theta_degrees(self):
        # DA和DE的夹角
        return 119.0

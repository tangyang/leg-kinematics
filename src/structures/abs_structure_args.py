from abc import ABCMeta, abstractmethod
from enum import unique, Enum

LINK_EF = "link_ef"

LINK_DE = "link_de"

LINK_BE = "link_be"

LINK_AB = "link_ab"

ORG_LINK_AC = "org_link_ac"

LINK_CD = "link_bc"

LINK_BF = "link_bf"
LINK_FB = "link_fb"


@unique
class KeyPoint(Enum):
    POINT_NONE = -1
    POINT_END = 0
    POINT_LEFT = 1
    POINT_RIGHT = 2
    POINT_FORWARD = 3

@unique
class LinkSide(Enum):
    LEFT = 1
    RIGHT = -1

ANGLE_ALPHA = "alpha"

ANGLE_BETA = "beta"

ANGLE_THETA = "theta"


class AbsStructureArgs(metaclass=ABCMeta):

    @abstractmethod
    def _get_init_links(self):
        pass

    @abstractmethod
    def _get_init_point_end(self):
        pass

    @abstractmethod
    def _get_init_theta_degrees(self):
        pass

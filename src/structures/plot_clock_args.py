from structures.abs_structure_args import *


class PlotClockStructureArgs(AbsStructureArgs):
    """
    马克笔夹爪直径12.60mm -> 半径6.3mm
    """

    def _get_init_point_end(self):
        return [0, 60]

    def _get_init_links(self):
        return {
            ORG_LINK_AC: 27.0,
            LINK_CD: 36.0,
            LINK_AB: 36.0,
            LINK_BE: 46.0,
            LINK_DE: 46.0,
            LINK_EF: 16.0,
        }.copy()

    def _get_init_theta_degrees(self):
        return 135.0

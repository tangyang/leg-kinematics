import traceback
from abc import abstractmethod

from PyQt5 import QtGui, QtCore
from PyQt5.QtGui import QPainter
from PyQt5.QtWidgets import QWidget


class BaseOverlay(QWidget):

    def __init__(self, parent):
        super(BaseOverlay, self).__init__(parent)

        self.green_line_pen = QtGui.QPen(QtCore.Qt.GlobalColor.green, 2, QtCore.Qt.PenStyle.DashLine)

    def paintEvent(self, event: QtGui.QPaintEvent) -> None:
        super(BaseOverlay, self).paintEvent(event)

        qp = QtGui.QPainter()

        try:
            qp.begin(self)
            # 尽可能消除锯齿边缘
            qp.setRenderHint(QPainter.Antialiasing)
            # 尽可能消除文本锯齿边缘
            qp.setRenderHint(QPainter.TextAntialiasing)
            # 启用线性插值算法以此来平滑图片
            qp.setRenderHint(QPainter.SmoothPixmapTransform)
            self.draw_content(qp)
        except Exception as e:
            print(e)
            traceback.print_exc()
        finally:
            qp.end()

    @abstractmethod
    def draw_content(self, qp):
        pass

    def draw_test_rect(self, qp):
        qp.setPen(self.green_line_pen)
        qp.setBrush(QtGui.QColor(255, 255, 0, 10))
        qp.drawRect(0, 0, self.width() - 0, self.height() - 0)

from PyQt5.QtWidgets import QDialog

from view.ui.ui_shape_line_dialog import Ui_ShapeLineDialog


class ShapeLineDialog(QDialog):
    def __init__(self, parent=None, default_values=None):
        super(ShapeLineDialog, self).__init__(parent)
        # self.setWindowTitle("Line")
        # self.setFixedSize(300, 200)

        self.ui = Ui_ShapeLineDialog()
        self.ui.setupUi(self)

        if default_values is not None:
            (start_x, start_y), (end_x, end_y) = default_values
            self.ui.spin_start_x.setValue(start_x)
            self.ui.spin_start_y.setValue(start_y)
            self.ui.spin_end_x.setValue(end_x)
            self.ui.spin_end_y.setValue(end_y)

    def get_values(self):
        return (self.ui.spin_start_x.value(), self.ui.spin_start_y.value()), (self.ui.spin_end_x.value(), self.ui.spin_end_y.value())

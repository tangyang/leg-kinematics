from PyQt5.QtWidgets import QDialog

from view.ui.ui_b_spline_dialog import Ui_BSplineDialog


class BSplineDialog(QDialog):

    def __init__(self, parent=None):
        super(BSplineDialog, self).__init__(parent)

        self.ui = Ui_BSplineDialog()
        self.ui.setupUi(self)

    def get_values(self):
        return self.ui.spinBox.value(), self.ui.spinBox_2.value()
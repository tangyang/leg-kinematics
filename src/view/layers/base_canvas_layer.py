from abc import ABCMeta, abstractmethod

import numpy as np
from PyQt5.QtCore import QPointF, QObject
from PyQt5.QtGui import QPainter
from PyQt5.QtWidgets import QWidget

from structures.basic_structure import BasicStructure


class BaseLayer(QObject):

    def __init__(self, ls: BasicStructure):
        super().__init__()
        self.visible = True
        self._ls = ls

    def set_visible(self, visible):
        self.visible = visible

    @abstractmethod
    def render(self, qp: QPainter, widget: QWidget):
        pass

    def draw_circle(self, qp, center: np.ndarray, radius):
        qp.drawEllipse(QPointF(center[0], center[1]), radius, radius)
import enum

from PyQt5 import QtGui, QtCore
from PyQt5.QtCore import QEvent, Qt, QLineF, QPointF
from PyQt5.QtGui import QPainter
from PyQt5.QtWidgets import QWidget

from view.layers.base_canvas_layer import BaseLayer


@enum.unique
class Shape(enum.Enum):
    """
        -1 表示没有绘制
        0 表示绘制的是线
        1 表示绘制的是圆
        2 表示绘制的是三角形
        3 表示绘制的是矩形
    """
    NONE = -1
    LINE = 0
    CIRCLE = 1
    TRIANGLE = 2
    RECTANGLE = 3


class ShapeLayer(BaseLayer):
    LAYER_NAME = "Shape"

    # 定义形状完成信号
    shape_finished_signal = QtCore.pyqtSignal(Shape, list)

    def __init__(self, ls):
        super(ShapeLayer, self).__init__(ls)
        self._shape = None
        self.visible = True
        self.__cursor_point = None
        self._record_points = []

        self._record_pen = QtGui.QPen(QtCore.Qt.GlobalColor.red, 1, QtCore.Qt.PenStyle.SolidLine)
        self._record_pen_dash = QtGui.QPen(QtCore.Qt.GlobalColor.magenta, 2, QtCore.Qt.PenStyle.DashLine)
        self._record_keypoint_pen = QtGui.QPen(QtCore.Qt.GlobalColor.blue, 3, QtCore.Qt.PenStyle.SolidLine)

    def set_shape(self, shape: Shape):
        self._shape = shape
        if shape == Shape.NONE:
            self._record_points = []

    def _append_point(self, cursor):
        self._record_points.append(cursor)

    def remove_last_point(self):
        if len(self._record_points) > 0:
            self._record_points.pop()
        return True

    def handle_drawing_event(self, event, x, y):
        cursor = [x, y]
        self.__cursor_point = cursor

        if event.type() == QEvent.MouseButtonPress:
            if event.button() == Qt.LeftButton:
                self._append_point(cursor)
                self.try_emit()
                return True
            elif event.button() == Qt.RightButton:
                return self.remove_last_point()

        elif event.type() == QEvent.MouseMove:
            if len(self._record_points) == 0:
                return False

            return True

        return False

    def try_emit(self):
        if self._shape == Shape.LINE:
            if len(self._record_points) >= 2:
                real_points = [self._ls.inverse_ratio(p) for p in self._record_points]
                self.shape_finished_signal.emit(self._shape, real_points)

    def render(self, qp: QPainter, widget: QWidget):
        if not self.visible:
            return
        # 根据shape确定绘制的方式
        if self._shape == Shape.LINE:
            self.__draw_line(qp, widget)
        elif self._shape == Shape.CIRCLE:
            self.__draw_circle(qp, widget)
        elif self._shape == Shape.TRIANGLE:
            self.__draw_triangle(qp, widget)
        elif self._shape == Shape.RECTANGLE:
            self.__draw_rectangle(qp, widget)

    def __draw_line(self, qp: QPainter, widget):

        points = self._record_points
        if len(points) == 0:
            return

        # 绘制线
        qp.setPen(self._record_pen)
        for i in range(1, len(points)):
            qp.drawLine(QLineF(*points[i - 1], *points[i]))

        # 绘制点
        qp.setPen(self._record_keypoint_pen)
        qp.drawPoints(*[QPointF(p[0], p[1]) for p in points])

        if self.__cursor_point is not None and len(points) < 2:
            # 绘制最后一个点到鼠标的虚线
            qp.setPen(self._record_pen_dash)
            qp.drawLine(QPointF(*points[-1]), QPointF(*self.__cursor_point))

    def __draw_circle(self, qp: QPainter, widget):
        pass

    def __draw_triangle(self, qp: QPainter, widget):
        pass

    def __draw_rectangle(self, qp: QPainter, widget):
        pass

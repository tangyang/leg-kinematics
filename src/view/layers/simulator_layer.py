from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import Qt, QPointF, QRectF
from PyQt5.QtGui import QPainter, QPen
from PyQt5.QtWidgets import QWidget

from structures.basic_structure import *
from view.layers.base_canvas_layer import BaseLayer


class SimulatorLayer(BaseLayer):
    LAYER_NAME = "Simulator"

    def __init__(self, ls: BasicStructure):
        super(SimulatorLayer, self).__init__(ls)

        self.green_line_pen = QtGui.QPen(QtCore.Qt.GlobalColor.green, 2, QtCore.Qt.PenStyle.DotLine)
        self.radar_green_pen = QtGui.QPen(QtCore.Qt.GlobalColor.green, 2, QtCore.Qt.PenStyle.SolidLine)

        self.black_pen = QtGui.QPen(QtCore.Qt.GlobalColor.black, 3, QtCore.Qt.PenStyle.SolidLine)

        self.red_pen = QtGui.QPen(QtCore.Qt.GlobalColor.red, 2, QtCore.Qt.PenStyle.SolidLine)
        self.red_brush = QtGui.QColor(255, 100, 0)
        self.blue_brush = QtGui.QColor("#1c7ed6")
        self.q_font = QtGui.QFont('Consolas', 10, QtGui.QFont.Bold)

        self.point_text_bg_brush = QtGui.QColor(172, 119, 176, 100)

        self.green_pen = QtGui.QPen(QtCore.Qt.GlobalColor.green, 2, QtCore.Qt.PenStyle.SolidLine)
        self.green_pen_2 = QtGui.QPen(QtCore.Qt.GlobalColor.darkGreen, 3, QtCore.Qt.PenStyle.SolidLine)

        self.circle_pen = QtGui.QPen(QtCore.Qt.GlobalColor.blue, 1, QtCore.Qt.PenStyle.DashLine)

        # 扇形填充
        self.arc_brush = QtGui.QColor(120, 230, 160, 100)
        # 圆点半径
        self.point_radius = 5

    def draw_point(self, qp: QPainter, point, name=''):
        disable_drag = ['A', 'C', 'E']

        qp.setPen(self.red_pen)
        if name in disable_drag:
            qp.setBrush(QtCore.Qt.GlobalColor.red)
        else:
            qp.setBrush(self.red_brush)

        self.draw_circle(qp, point, self.point_radius)

        qp.setPen(self.black_pen)
        qp.setFont(self.q_font)
        qp.drawText(QPointF(point[0] + 5, point[1] - 5), name)

    def render(self, qp: QPainter, widget: QWidget):
        # 圆环
        qp.setPen(self.circle_pen)
        qp.setBrush(Qt.GlobalColor.transparent)

        self._ls.is_link_visible(LINK_AB) and self.draw_circle(qp, self._ls.point_a, self._ls.links[LINK_AB])
        self._ls.is_link_visible(LINK_CD) and self.draw_circle(qp, self._ls.point_c, self._ls.links[LINK_CD])
        self._ls.is_link_visible(LINK_BE) and self.draw_circle(qp, self._ls.point_b, self._ls.links[LINK_BE])
        self._ls.is_link_visible(LINK_DE) and self.draw_circle(qp, self._ls.point_d, self._ls.links[LINK_DE])
        self._ls.is_link_visible(LINK_EF) and self.draw_circle(qp, self._ls.point_e, self._ls.links[LINK_EF])

        # self._ls.is_link_visible(LINK_BF) and self.draw_circle(qp, self._ls.point_b, self._ls.links[LINK_BF])
        # self._ls.is_link_visible(LINK_FB) and self.draw_circle(qp, self._ls.point_f, self._ls.links[LINK_BF])

        # 连杆
        qp.setPen(self.green_pen)
        qp.drawLine(QPointF(*self._ls.point_a), QPointF(*self._ls.point_b))
        qp.drawLine(QPointF(*self._ls.point_c), QPointF(*self._ls.point_d))
        qp.drawLine(QPointF(*self._ls.point_d), QPointF(*self._ls.point_e))
        qp.setPen(self.green_pen_2)
        qp.drawLine(QPointF(*self._ls.point_b), QPointF(*self._ls.point_e))
        qp.drawLine(QPointF(*self._ls.point_e), QPointF(*self._ls.point_f))

        # QtGui.QPen(QtCore.Qt.GlobalColor.green, 2, QtCore.Qt.PenStyle.SolidLine)
        qp.setPen(QPen(Qt.NoPen))
        qp.setBrush(self.arc_brush)

        # 角度扇形
        mode = self._ls.is_left_mode()

        center, radius = self._ls.point_a, self._ls.links[LINK_AB]
        qp.drawPie(QRectF(center[0] - radius, center[1] - radius, radius * 2, radius * 2),
                   (180 if mode else 0) * 16,
                   self._ls.solid_link_side.value * int(np.rad2deg(self._ls.alpha_radians) * 16))

        center, radius = self._ls.point_c, self._ls.links[LINK_CD]
        qp.drawPie(QRectF(center[0] - radius, center[1] - radius, radius * 2, radius * 2),
                   (0 if mode else 180) * 16,
                   self._ls.solid_link_side.value * int(np.rad2deg(self._ls.beta_radians) * 16))

        self.draw_drag_line(qp)

        e_suffix = "（{:.1f}, {:.1f}）mm".format(*self._ls.inverse_ratio(self._ls.point_f))
        a_suffix = "（α: {:.1f}°）".format(np.rad2deg(self._ls.alpha_radians))
        c_suffix = "（β: {:.1f}°）".format(-np.rad2deg(self._ls.beta_radians))
        # 点
        self.draw_point(qp, self._ls.point_a, 'A')
        self.draw_point(qp, self._ls.point_c, 'C')
        self.draw_point(qp, self._ls.point_f, 'F' + e_suffix)
        self.draw_point(qp, self._ls.point_b, 'B' + a_suffix)
        self.draw_point(qp, self._ls.point_e, 'E')
        self.draw_point(qp, self._ls.point_d, 'D' + c_suffix)

        # 角度文字
        # qp.setPen(self.black_pen)
        # qp.drawText(QPointF(self._ls.point_a[0] - 30, self._ls.point_a[1] - 15),
        #             "α: {:.1f}°".format(np.rad2deg(self._ls.alpha_radians)))
        # qp.drawText(QPointF(self._ls.point_c[0] - 10, self._ls.point_c[1] - 15),
        #             "β: {:.1f}°".format(-np.rad2deg(self._ls.beta_radians)))

    def draw_drag_line(self, qp):
        """
        绘制拖动线
        :param qp:
        :return:
        """
        if self._ls.drag_line is None:
            return

        qp.setPen(self.green_line_pen)

        start_point, end_point = self._ls.drag_line[0], self._ls.drag_line[1]

        qp.drawLine(QPointF(*start_point), QPointF(*end_point))

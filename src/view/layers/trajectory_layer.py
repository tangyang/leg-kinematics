from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import QPointF, QRectF
from PyQt5.QtGui import QPainter, QColor, QPolygonF
from PyQt5.QtWidgets import QWidget

from structures.basic_structure import BasicStructure
from trajectories.traj_base import BaseTrajectory
from view.layers.base_canvas_layer import BaseLayer


class TrajectoryLayer(BaseLayer):
    LAYER_NAME = "Trajectory"
    """
    绘制轨迹层
    """

    def __init__(self, ls: BasicStructure):
        super(TrajectoryLayer, self).__init__(ls)

    def render(self, qp: QPainter, widget: QWidget):
        self._render_walks(qp)
        self._render_shapes(qp)

        # self.draw_number(qp)

    def draw_number(self, qp):
        # 绘制一个数字9
        # 设置字体 粗体
        x, y, w, h = self._ls.forward_ratio([0, 50, 15, 20])
        qp.setPen(QtGui.QPen(QColor(160, 0, 70, 120), 1, QtCore.Qt.PenStyle.SolidLine))
        qp.setBrush(QtCore.Qt.NoBrush)
        qp.drawRect(QRectF(x, y, w, h))
        qp.setPen(QtGui.QPen(QColor(0, 0, 0, 60), 1, QtCore.Qt.PenStyle.SolidLine))
        # qp.setFont(QtGui.QFont("Arial", int(self._ls.forward_ratio(20)), QtGui.QFont.Bold))
        qp.setFont(QtGui.QFont("Cascadia Mono", int(self._ls.forward_ratio(20))))
        qp.drawText(QRectF(x, y - 30, w, h + 30), "0")

    def _render_walks(self, qp):
        trajectory: BaseTrajectory = self._ls.get_walks_trajectory()
        if trajectory is None:
            return

        qp.setPen(QtGui.QPen(QColor(*trajectory.get_color()), 1, QtCore.Qt.PenStyle.SolidLine))
        qp.drawPoints(QPolygonF([QPointF(p[0], p[1]) for p in trajectory.get_path_points() if len(p) > 1]))

    def _render_shapes(self, qp):
        trajectory_list = self._ls.get_trajectory_list()

        for trajectory in trajectory_list:
            qp.setPen(QtGui.QPen(QColor(*trajectory.get_color()), 2, QtCore.Qt.PenStyle.SolidLine))
            qp.drawPoints(QPolygonF([QPointF(p[0], p[1]) for p in trajectory.get_path_points() if len(p) > 1]))

"""
初始化FourLegWindow窗口
"""
import json
from PyQt5.QtWidgets import QMainWindow, QMessageBox

from trajectories.traj_forward import ForwardTrajectory
from view.ui.ui_four_leg_window import Ui_FourLegWindow

rear_default_angles = "[[90.4, 17.1], [90.6, 16.0], [89.1, 15.4], [86.3, 15.4], [82.5, 16.0], [78.1, 17.0], [73.3, 18.4], [68.4, 20.0], [63.6, 21.8], [58.9, 23.7], [54.4, 25.9], [50.2, 28.1], [46.5, 30.3], [43.2, 32.5], [40.4, 34.7], [38.2, 36.8], [36.2, 38.8], [34.6, 40.8], [33.2, 42.8], [32.0, 44.8], [30.9, 46.8], [29.9, 48.8], [29.1, 50.9], [28.4, 52.8], [28.0, 54.7], [27.7, 56.5], [27.7, 58.1], [27.8, 59.6], [28.1, 60.9], [28.4, 61.9], [28.8, 62.6], [29.2, 63.0], [29.6, 63.1], [30.0, 62.7], [30.4, 62.2], [30.7, 61.3], [31.1, 60.4], [31.5, 59.3], [31.8, 58.2], [32.2, 57.1], [32.6, 56.0], [33.1, 55.0], [33.5, 54.0], [33.9, 53.0], [34.4, 52.1], [34.8, 51.2], [35.3, 50.4], [35.9, 49.5], [36.5, 48.7], [37.3, 47.8], [38.2, 46.9], [39.4, 45.9], [40.7, 44.9], [42.2, 43.9], [43.8, 42.9], [45.5, 41.8], [47.2, 40.9], [49.0, 40.0], [50.8, 39.1], [52.6, 38.3], [54.4, 37.5], [56.2, 36.7], [58.1, 35.9], [60.1, 35.0], [62.2, 34.0], [64.8, 32.5], [68.3, 30.2], [73.1, 26.9], [80.1, 22.4], [90.4, 17.1], [999, 999]]"

front_default_angles = "[[22.8, 82.3], [26.5, 79.5], [29.0, 76.9], [30.5, 74.6], [31.2, 72.6], [31.3, 70.7], [31.3, 69.0], [31.3, 67.4], [31.4, 65.9], [31.7, 64.6], [32.1, 63.2], [32.7, 61.8], [33.6, 60.4], [34.7, 58.9], [36.0, 57.3], [37.5, 55.6], [39.2, 53.9], [41.0, 52.3], [42.9, 50.7], [44.8, 49.2], [46.6, 47.8], [48.3, 46.6], [50.1, 45.3], [51.9, 44.2], [53.7, 43.0], [55.7, 41.9], [57.8, 40.8], [60.0, 39.6], [62.2, 38.5], [64.4, 37.2], [66.6, 35.8], [68.6, 34.2], [70.4, 32.5], [72.0, 30.6], [73.2, 28.7], [73.9, 27.0], [74.0, 25.4], [73.4, 24.3], [71.9, 23.6], [69.5, 23.6], [66.5, 24.1], [63.0, 25.1], [59.2, 26.4], [55.4, 28.0], [51.8, 29.7], [48.3, 31.5], [45.0, 33.2], [41.9, 35.1], [38.9, 37.0], [36.0, 39.1], [33.3, 41.3], [30.6, 43.9], [28.2, 46.5], [26.0, 49.4], [24.1, 52.2], [22.5, 55.1], [21.2, 58.0], [20.3, 60.7], [19.6, 63.4], [19.1, 65.9], [18.8, 68.4], [18.6, 70.9], [18.6, 73.3], [18.8, 75.7], [19.0, 78.1], [19.4, 80.2], [19.9, 81.9], [20.7, 83.0], [21.6, 83.2], [22.8, 82.3], [999, 999]]"


class FourLegWindow(QMainWindow):
    """
    初始化FourLegWindow窗口
    """

    def __init__(self):
        super().__init__()
        self.ui = Ui_FourLegWindow()
        self.ui.setupUi(self)
        self.setWindowTitle("四足机器人")

        self.ui.edit_angles_1.setText(front_default_angles)
        self.ui.edit_angles_2.setText(front_default_angles)
        self.ui.edit_angles_3.setText(rear_default_angles)
        self.ui.edit_angles_4.setText(rear_default_angles)

        self.widget_dict = {
            1: [self.ui.widget_1, self.ui.edit_angles_1, self.ui.spin_delay_1, self.ui.btn_run_1],
            2: [self.ui.widget_2, self.ui.edit_angles_2, self.ui.spin_delay_2, self.ui.btn_run_2],
            3: [self.ui.widget_3, self.ui.edit_angles_3, self.ui.spin_delay_3, self.ui.btn_run_3],
            4: [self.ui.widget_4, self.ui.edit_angles_4, self.ui.spin_delay_4, self.ui.btn_run_4],
        }

        for k, v in self.widget_dict.items():
            v[3].clicked.connect(lambda event, leg_id=k: self.run(leg_id))

        self.ui.btn_run_all.clicked.connect(self.run_all)

    def run_all(self):
        run_flag = True

        for k, v in self.widget_dict.items():
            run_rst = self.run(k)
            run_flag = run_flag and run_rst

        self.ui.btn_run_all.setText("Stop All")
        if run_flag:
            return

        # 如果有未成功执行的，则说明有轨迹启动失败，则全部停止
        for k, v in self.widget_dict.items():
            self.run(k, stop=True)

        self.ui.btn_run_all.setText("Run All")

    def run(self, leg_id, stop=False):
        print("run leg_id: ", leg_id)
        leg_widget, edit_angles, spin_delay, btn_run = self.widget_dict[leg_id]

        if stop or leg_widget.trajectory_running():
            leg_widget.trajectory_clean()
            btn_run.setText("Run")
            return False

        btn_run.setText("Stop")

        # 读取edit_angles_2的值
        json_text = edit_angles.text()
        delay_value = spin_delay.value()

        try:
            # 将angles解析json字符串为列表
            angles_list = json.loads(json_text)
            if angles_list is None or len(angles_list) == 0:
                QMessageBox.information(self, "加载失败", f"加载失败，请检查{leg_id}位置json是否正确：{json_text}\n")
                return False

            # 根据 delay_value 的比例对 angles_list 的数据位置进行向右平移
            delay_count = int(len(angles_list) * delay_value / 100)
            new_angles_list = angles_list[delay_count:] + angles_list[:delay_count]

            leg_widget.trajectory_generate(ForwardTrajectory.TRACK_KEY, new_angles_list)

            return True
        except Exception as e:
            print(e)
            QMessageBox.information(self, "加载失败", f"加载失败，请检查{leg_id}位置json是否正确：{json_text}\n")

        return False

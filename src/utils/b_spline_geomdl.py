import numpy as np

from geomdl import BSpline
from geomdl import knotvector

def approximate_b_spline(ctrl_pts: list, degree: int = 3, count=100) -> tuple:
    crv = BSpline.Curve()
    # Set degree
    crv.degree = degree
    # Set control points
    crv.ctrlpts = ctrl_pts
    # Generate a uniform knot vector
    crv.knotvector = knotvector.generate(crv.degree, crv.ctrlpts_size)
    # Set evaluation delta (optional)
    # 根据此参数，可以控制计算精度，默认为0.01，数值越小得到的点数越多
    crv.delta = 1.0 / count

    # Get curve points
    points = crv.evalpts

    print(len(points))
    return points


def approximate_b_spline_path(x: list, y: list, degree: int = 3, count=100) -> tuple:
    crv = BSpline.Curve()
    # Set degree
    crv.degree = degree
    ctrl_pts = np.vstack((x, y)).T
    # Set control points
    crv.ctrlpts = ctrl_pts.tolist()
    # Generate a uniform knot vector
    crv.knotvector = knotvector.generate(crv.degree, crv.ctrlpts_size)
    # Set evaluation delta (optional)
    # 根据此参数，可以控制计算精度，默认为0.01，数值越小得到的点数越多
    crv.delta = 1.0 / count

    # Get curve points
    points = crv.evalpts

    print(len(points))
    # print(points)
    rax = np.array(points)[:, 0]
    ray = np.array(points)[:, 1]
    return rax, ray

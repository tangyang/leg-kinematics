import numpy as np

from utils.math_tool import calc_slice_count


def base_test():
    a = [1, 3]
    b = [2, 4]
    c = [1, 3]
    print(a == b)
    print(a == c)
    print(round(1.34, 3))
    print(round(1.56, 2))
    print(round(1.67, 1))
    print(round(1.78, 0))
    print(round(1.8))


def main():
    start_point, end_point = np.array([
        [0, 0],
        [100, 0],
    ])
    length = int(np.linalg.norm(end_point - start_point))
    s_count = calc_slice_count(length)
    slice_count = np.max([s_count, 2])
    slice_count = s_count
    slice_time_second = 1 / slice_count
    linspace_points = np.linspace(start_point, end_point, num=slice_count)
    print(f"length: {length} count: {slice_count} sleep: {slice_time_second}")
    print(linspace_points)


def linspace_test():
    start_point, end_point = np.array([
        [0, 0],
        [100, 0],
    ])
    linspace_points = np.linspace(start_point, end_point, num=1)
    print(linspace_points)


if __name__ == '__main__':
    # main()
    base_test()
    # linspace_test()

    # a_dict = {
    #     "a": 1,
    #     "b": 2,
    #     "c": 3,
    #     "d": 4,
    # }
    # rst = { k : v * 2 for k, v in a_dict.items()}
    # print(rst)
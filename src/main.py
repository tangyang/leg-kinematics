import sys
import argparse
from PyQt5.QtWidgets import QApplication
from view.main_window import MainWindow


def main():
    """
    Main function.
    启动方式 python main.py -h
    以下是可以设置的参数：
    -h, --help
    -v, --version
    -m, --mode 启动模式，可选值：
        1. 狗腿子模式，默认值
        2. 小贱钟模式

    :return:
    """

    app = QApplication(sys.argv)

    # 使用 argparse 解析启动参数
    parser = argparse.ArgumentParser(description='模拟器默认启动模式')
    parser.add_argument('-m', '--mode', type=int, default=1, help='启动模式，可选值：1. 狗腿子模式，默认值 2. 小贱钟模式')
    args = parser.parse_args()

    main_window = MainWindow(args.mode)

    app.installEventFilter(main_window)

    main_window.show()
    exit_code = app.exec_()
    print("------------exit_code: ", exit_code)


if __name__ == '__main__':

    main()

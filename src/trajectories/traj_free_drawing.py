import time

from structures.basic_structure import BasicStructure
from trajectories.traj_base import BaseTrajectory


class FreeDrawingTrajectory(BaseTrajectory):
    TRACK_KEY = "free_drawing"

    def get_track_key(self):
        return self.TRACK_KEY

    def get_color(self):
        return [160, 76, 247]

    def tracking(self, args, update_cb, ls: BasicStructure):
        if args is None or len(args) == 0:
            return

        record_points_list = args
        print('record_points_list: ', record_points_list)
        for key_points_segment in record_points_list:
            for key_points in key_points_segment:
                if len(key_points) == 1:
                    p = key_points[0]
                    if ls.try_update_point_end(p):
                        alpha, beta = ls.inverse_not_update(p)
                        self._add_angles((alpha, beta))
                        self._add_point(p)

                        self._add_angles((999, 999))
                        self._add_point(ls.forward_ratio([999, 55]))

                        update_cb()
                        time.sleep(0.01)

                    continue

                for i in range(len(key_points) - 1):
                    start_point = key_points[i]
                    end_point = key_points[i + 1]
                    # print('start_point: {}, end_point: {}'.format(start_point, end_point))
                    self._track_line_points(start_point, end_point, update_cb, ls)


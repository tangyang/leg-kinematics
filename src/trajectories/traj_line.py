import numpy as np

from trajectories.traj_base import BaseTrajectory


class LineTrajectory(BaseTrajectory):
    TRACK_KEY = "line"

    def get_track_key(self):
        return self.TRACK_KEY

    def get_color(self):
        return [255, 54, 51]

    def tracking(self, args, update_cb, ls):
        start_point, end_point = ls.forward_ratio(np.array(args))

        self._track_line_points(start_point, end_point, update_cb, ls)

        self._add_angles((999, 999))
        self._add_point(ls.forward_ratio([999, 55]))
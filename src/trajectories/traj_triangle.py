import numpy as np

from trajectories.traj_base import BaseTrajectory


class TriangleTrajectory(BaseTrajectory):
    TRACK_KEY = "triangle"

    def get_track_key(self):
        return self.TRACK_KEY

    def get_color(self):
        return [255, 54, 255]


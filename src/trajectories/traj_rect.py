import time

import numpy as np

from structures.basic_structure import BasicStructure
from trajectories.traj_base import BaseTrajectory


class RectTrajectory(BaseTrajectory):
    TRACK_KEY = "rect"

    def get_track_key(self):
        return self.TRACK_KEY

    def get_color(self):
        return [5, 200, 80]

    def tracking(self, args, update_cb, ls):
        start_point, width, height = args

        start_point = ls.forward_ratio(np.array(start_point))
        width = ls.forward_ratio(width)
        height = ls.forward_ratio(height)

        point1 = start_point + np.array([width, 0])
        point2 = start_point + np.array([width, height])
        point3 = start_point + np.array([0, height])

        self._track_line_points(start_point, point1, update_cb, ls)
        self._track_line_points(point1, point2, update_cb, ls)
        self._track_line_points(point2, point3, update_cb, ls)
        self._track_line_points(point3, start_point, update_cb, ls)

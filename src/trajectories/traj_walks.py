import time

from structures.basic_structure import BasicStructure
from trajectories.traj_base import BaseTrajectory


class WalksTrajectory(BaseTrajectory):

    TRACK_KEY = "walks"

    def get_track_key(self):
        return self.TRACK_KEY

    def get_color(self):
        return [40, 26, 51, 188]

    def save(self, ls):
        ls.set_walks_trajectory(self)

    def tracking(self, args, update_cb, ls: BasicStructure):

        degrees_start, degrees_end = args

        direction = 1
        division_scale = 1.0

        start_degrees = int(degrees_start * division_scale)
        divisions = int(degrees_end * division_scale)
        for i in range(start_degrees, divisions):
            # 更新alpha度数
            ls.try_update_alpha(i * (1 / division_scale))

            range_list = range(start_degrees, divisions) if direction == 1 else range(divisions, start_degrees, -1)

            for j in range_list:

                # 不要让两个杆子挨得太近（否则会出现跳跃）
                # 也不要让杆子都跑上边
                if i + j > 180 or i + j < -10:
                    continue

                # 更新beta度数
                if ls.try_update_beta(j * (1 / division_scale)):

                    if not self.is_running:
                        return False

                    # 记录轨迹
                    self._add_point(ls.point_f)

                    update_cb()
                    # time.sleep(0.0001)

            direction *= -1

        return True
